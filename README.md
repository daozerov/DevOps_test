Востребованные команды Ubuntu/Windows используемые в рамках тестового задания

# Работа с каталогами/папкам Ubuntu
$ mkdir [имя каталога] — создает новый каталог/папку;

$ rmdir [имя каталога] — удаляет каталог/папку;

$ cd [имя каталога] — переход в домашний каталог;

$ ls -list — просмотр содержимого текущего каталога;

# Сетевые команды Ubuntu
$ ip a или ifconfig — узнать IP адрес машины/сервера;

# Обновление и установка пакетов Ubuntu
$ sudo apt-get update — обновляет список доступных пакетов из интернета;

$ sudo apt-get upgrade — обновляется доступная версия установленных пакетов в системе;

# Docker for Windows
$ pip install docker — установка Docker;

$ docker build -t [имя образа] . — создаёт новый образ;

$ docker images — показывает все образы;

$ docker ps — показывает список запущенных контейнеров;

$ docker start [имя контейнера] — запускает контейнер;

$ docker stop [имя контейнера] — останавливает запрущенный контейнер;

# Python for Ubuntu
$ pip --version — проверка версии системы управления пакетами PIP;

$ python3 --version — проверить установлен ли Python 3 версии;

$ sudo apt-get -y install python3-pip — установка пакета PIP на Python 3 версии;

# Git for Ubuntu
$ sudo apt-get install git python-pip — установка Git и Python;

$ git clone [ссылка на репозиторий] — клонирует существующий репозиторий;

$ git init – создает новый репозиторий;

$ git status – отображает список измененных, добавленных и удаленных файлов;

$ git add – добавляет указанные файлы в индекс;

$ git commit – фиксирует добавленные в индекс изменения;

# Ansible for Ubuntu
$ sudo apt-get install ansible — установка Ansible;

$ ansible-playbook [имя плейбука] — запуск готового плейбука;

# SSH - доступ к удалённому серверу
$ ssh-keygen или sudo ssh-keygen -t rsa — создает RSA ключ;

$ sudo cat ~/.ssh/id_rsa.pub | ssh username@remote_host "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys" — скопировать ключа по ssh с одной машины на другую машину;
